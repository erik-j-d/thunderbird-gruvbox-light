# Gruvbox-Light Theme for Thunderbird

## Installation Instructions

Run:

```
$ git clone https://codeberg.org/erik-j-d/thunderbird-gruvbox-light.git
$ cd thunderbird-gruvbox-light
$ make xpi
```

Then [install the generated .xpi file into thunderbird](https://support.mozilla.org/en-US/kb/installing-addon-thunderbird#w_a-slightly-less-ideal-case-install-from-a-downloaded-xpi-file).
